<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Result extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'match_id','team_a_goals','team_b_goals'
  ];

  /* RELATIONSHIPS */


  /**
   * Get the match of the Result
   */
  public function match()
  {
      return $this->belongsTo('App\Match','match_id');
  }

  /**
  * Get players of the Result
  */
  public function players()
    {
        return $this->belongsToMany('App\Player',"players_results")
            ->withPivot("gol", "red_cards", "yellow_cards");
    }


    /**@author Lupita Llamas
     * Realiza el query para traer las estadísticas de un equipo en slot A, segun su torneo en jornada Regular
     * @param $team_id
     * @param $tournament_id
     * @return \Illuminate\Support\Collection
     */
  public static function getTeamAStats($team_id, $tournament_id) {
      return DB::table('results')
          ->select(DB::raw('COUNT(*) as matches_played'),
              DB::raw('SUM(CASE WHEN results.team_a_goals > results.team_b_goals THEN 1 ELSE 0 END ) as matches_won'),
              DB::raw('SUM(CASE WHEN results.team_a_goals < results.team_b_goals THEN 1 ELSE 0 END ) as matches_lost'),
              DB::raw('SUM(CASE WHEN results.team_a_goals = results.team_b_goals THEN 1 ELSE 0 END ) as draw_matches'),
              DB::raw('SUM(results.team_a_goals) as goals'),
              DB::raw('SUM(results.team_b_goals) as received_goals'))
          ->leftJoin('matches', 'matches.id', '=', 'results.match_id')
          ->leftJoin('sets', 'sets.id', '=', 'matches.set_id')
          ->where('sets.type', 'regular')
          ->where('matches.team_a_id', $team_id)
          ->where('matches.tournament_id', $tournament_id)
          ->get();
  }

    /**@author Lupita Llamas
     * Realiza el query para traer las estadísticas de un equipo en slot B, segun su torneo en jornada Regular
     * @param $team_id
     * @param $tournament_id
     * @return \Illuminate\Support\Collection
     */
  public static function getTeamBStats($team_id, $tournament_id) {
      return DB::table('results')
          ->select(DB::raw('COUNT(*) as matches_played'),
              DB::raw('SUM(CASE WHEN results.team_b_goals > results.team_a_goals THEN 1 ELSE 0 END ) as matches_won'),
              DB::raw('SUM(CASE WHEN results.team_b_goals < results.team_a_goals THEN 1 ELSE 0 END ) as matches_lost'),
              DB::raw('SUM(CASE WHEN results.team_b_goals = results.team_a_goals THEN 1 ELSE 0 END ) as draw_matches'),
              DB::raw('SUM(results.team_b_goals) as goals'),
              DB::raw('SUM(results.team_a_goals) as received_goals'))
          ->leftJoin('matches', 'matches.id', '=', 'results.match_id')
          ->leftJoin('sets', 'sets.id', '=', 'matches.set_id')
          ->where('sets.type', 'regular')
          ->where('matches.team_b_id', $team_id)
          ->where('matches.tournament_id', $tournament_id)
          ->get();
  }
}
