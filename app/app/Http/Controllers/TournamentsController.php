<?php

namespace App\Http\Controllers;

use App\Match;
use App\Result;
use App\Team;
use App\Tournament;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Log;


class TournamentsController extends Controller
{

    /*
      Estadisticas de los equipos en un torneo
      las cuales se obtienen solo de las jornadas
      regulares (omiten las jornadas extras), el
      arreglo tiene que estár ordenado del equipo
      que tiene mas puntos(points) al que menos
      [
        {
        {
          "id": 1,                     // id de equipo
          "name": "Leones",            // Nombre de equipo
          "goals": 4,                  // Goles totales del equipo en el torneo
          "received_goals": 5,         // Goles recibidos del equipo en el torneo
          "difference_of_goals": -1,   // Diferencia de goles (goals-received_goals)
          "matches_played": 6,         // El numero total de "matches" con resultado donde participa el equipo en el torneo
          "matches_won": 1,            // El numero total de "matches" con resultado donde participa el equipo en el torneo donde el equipo sea ganador
          "draw_matches": 2,           // El numero total de "matches" con resultado donde participa el equipo en el torneo donde el resultado sea empate
          "matches_lost": 3,           // El numero total de "matches" con resultado donde participa el equipo en el torneo donde el equipo sea el perdedor
          "points": 5,                 // Puntos obtenidos por el equipo en el torneo (3 puntos por partid ganado 1 punto por partido empatado y 0 puntos por partido perdido)
        }
        }
      ]
    */

    public function generalTable(Tournament $tournament){
        try{
            $teams = Team::select('teams.id', 'teams.name')
                ->leftJoin('tournament_team', 'tournament_team.team_id', '=', 'teams.id')
                ->where('tournament_team.tournament_id', $tournament->id)
                ->get();

            foreach ($teams as $team) {

                $matchesA = Result::getTeamAStats($team->id, $tournament->id);
                $matchesB = Result::getTeamBStats($team->id, $tournament->id);

                $team->matches_played = $matchesA[0]->matches_played + $matchesB[0]->matches_played;
                $team->matches_won = ($matchesA[0]->matches_won != NULL ? $matchesA[0]->matches_won:0) + ($matchesB[0]->matches_won != NULL ? $matchesB[0]->matches_won:0);
                $team->draw_matches = ($matchesA[0]->draw_matches != NULL ? $matchesA[0]->draw_matches:0) + ($matchesB[0]->draw_matches != NULL ? $matchesB[0]->draw_matches:0);
                $team->matches_lost = ($matchesA[0]->matches_lost != NULL ? $matchesA[0]->matches_lost:0) + ($matchesB[0]->matches_lost != NULL ? $matchesB[0]->matches_lost:0);
                $team->goals = ($matchesA[0]->goals != NULL ? $matchesA[0]->goals:0) + ($matchesB[0]->goals != NULL ? $matchesB[0]->goals:0);
                $team->received_goals = ($matchesA[0]->received_goals != NULL ? $matchesA[0]->received_goals:0) + ($matchesB[0]->received_goals != NULL ? $matchesB[0]->received_goals:0);
                $team->difference_of_goals = $team->goals - $team->received_goals;
                $team->points = ($team->matches_won * 3) + ($team->draw_matches);
            }

            $teams = $teams->sortByDesc('points');
            return response()->json($teams->values()->all(),200);
        }catch (\Exception $ex) {
            return response()->json(array('message' => $ex->getMessage()),500);
        }
    }


}
